# Smoky Eyes Example
![](screenshot.png)

This example uses the webcam and [ofxFaceTracker2](https://github.com/HalfdanJ/ofxFaceTracker2) and [ofxFluid](https://github.com/patriciogonzalezvivo/ofxFluid) to create smoke coming out of people's eyes. It is written with openFrameworks.

