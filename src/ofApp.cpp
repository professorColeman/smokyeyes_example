#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    // Setup grabber
    grabber.setup(1280,720);

	fluid.allocate(1280, 720, 0.5); //setup the fluid sim and use half resolution to improve fps

	// Seting the gravity and dissipation rates
	fluid.dissipation = 0.996;
	fluid.velocityDissipation = 0.99;
	fluid.setGravity(ofVec2f(0.0, 0.0));
    
    // All examples share data files from example-data, so setting data path to this folder
    // This is only relevant for the example apps
    ofSetDataPathRoot(ofFile("model/"));
    
    // Setup tracker
    tracker.setup();
}

//--------------------------------------------------------------
void ofApp::update(){ 

    grabber.update(); //update the camera
    
    // Update tracker when there are new frames
    if(grabber.isFrameNew()){
        tracker.update(grabber);
    }

	// Iterate over all faces
	for (auto face : tracker.getInstances()) { //go thru all faces found

		ofxFaceTracker2Landmarks markers = face.getLandmarks(); //get the landmarks for this face

		glm::vec2 lEyeT = markers.getImagePoint(38); //get the points for the top center of each eye
		glm::vec2 rEyeT = markers.getImagePoint(43);
		glm::vec2 lEyeB = markers.getImagePoint(40); //get the points for the top center of each eye
		glm::vec2 rEyeB = markers.getImagePoint(47);

		float lDist = ofDist(lEyeT.x, lEyeT.y, lEyeB.x, lEyeB.y);//calculate distance from top and bottom of eye
		float rDist = ofDist(rEyeT.x, rEyeT.y, rEyeB.x, rEyeB.y);

		lEye += (lEyeB - lEye) * .3; //easing the positions
		rEye += (rEyeB - rEye) * .3;

		float time = ofGetElapsedTimef();

		//make the colors shift with Noise
		ofFloatColor lColor(ofNoise(time / 1.1)*2.0 - 0.5, ofNoise(time / 0.3)*2.0 - 0.5, 1.0);
		ofFloatColor rColor(ofNoise(time / 0.7)*2.0 - 0.5, ofNoise(time / 1.2)*2.0 - 0.5, 1.0);

		//add the emmiters for each eye, using the distance to adjust size and flow. 
		fluid.addTemporalForce(lEye, ofPoint(sin(time /3.0), -lDist), lColor, lDist/2.0);
		fluid.addTemporalForce(rEye, ofPoint(sin(time / 4.0), -rDist), rColor, rDist/2.0);

	}

	//update the fluid simulation
	fluid.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
    // Draw camera image

	ofEnableAlphaBlending(); //we need to blend the camer and fluid
	grabber.draw(0, 0); //draw the camera
	fluid.draw(); //draw the fluid
	ofDisableAlphaBlending();
    

	/*  //This is for debugging that we have the right points on the eyes
for (auto face : tracker.getInstances()) { //go thru all faces found

	ofxFaceTracker2Landmarks markers = face.getLandmarks(); //get the landmarks for this face

	glm::vec2 lEyeT = markers.getImagePoint(38); //get the points for the top center of each eye
	glm::vec2 rEyeT = markers.getImagePoint(43);
	glm::vec2 lEyeB = markers.getImagePoint(40); //get the points for the top center of each eye
	glm::vec2 rEyeB = markers.getImagePoint(47);

	ofDrawRectangle(lEyeT, 2, 2);
	ofDrawRectangle(lEyeB, 2, 2);
	ofDrawRectangle(rEyeT, 2, 2);
	ofDrawRectangle(rEyeB, 2, 2);
}
*/
    
    ofDrawBitmapStringHighlight("Tracker fps: "+ofToString(tracker.getThreadFps()), 10, 20);
}
